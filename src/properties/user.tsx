/**
 * UserProps property interface.
 * @publicKey user public key for persistance
 * @userId user identification
 * @hisSecret paraphrase
 */
export interface UserProps {
    publicKey: string;
    userId: string;
    hisSecret: string;
}