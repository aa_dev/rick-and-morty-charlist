import React from "react";
import TextField from '@material-ui/core/TextField';

export const CharactersList: React.FC = () => {
    return (<div className="chars-container">
        <p>Rick and Morty characters list</p>
        <div className="chars-space">
            <div className="char-lsit">
            <TextField color="secondary" id="outlined-basic" label="Name" variant="outlined" helperText="Try snuffles"/>
                <ul>
                    <li><a href="http://"></a></li>
                    <li><a href="http://"></a></li>
                    <li><a href="http://"></a></li>
                    <li><a href="http://"></a></li>
                </ul>
            </div>
            <div className="char-display">
                <p>rick</p>
                <img src="" alt="rick" />
            </div>
        </div>
    </div>)
}